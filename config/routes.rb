Rails.application.routes.draw do


  get '/user/22', to:'users#index'
# 井手さんの要望、QRコードから飛ぶらしい
  
  get 'passengers/index'

  get 'passengers/show'

  get 'passengers/create'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)


  get'/', to:'users#index'
  get'/users/list', to:'users#list'
  get'/users/category', to:'users#category'
  resources :users
  
  # 今回追加
  post 'users/:id', to:'users#show'
  
  resources :bookings
  resources :reservers
  # resources :passengers

  resources :charges

end

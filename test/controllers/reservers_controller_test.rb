require 'test_helper'

class ReserversControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get reservers_index_url
    assert_response :success
  end

  test "should get show" do
    get reservers_show_url
    assert_response :success
  end

  test "should get create" do
    get reservers_create_url
    assert_response :success
  end

end

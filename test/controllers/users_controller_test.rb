require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get users_home_url
    assert_response :success
  end

  test "should get list" do
    get users_list_url
    assert_response :success
  end

  test "should get product" do
    get users_product_url
    assert_response :success
  end

  test "should get payment" do
    get users_payment_url
    assert_response :success
  end

end

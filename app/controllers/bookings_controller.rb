class BookingsController < ApplicationController
    
    
    def show
        @reserver=Reserver.new
        @reserver.passengers.new
        @booking=Booking.includes(:product).find(params[:id])
        @number=@booking.number
        @product=Product.find(@booking.product_id)
        @error=flash[:error]
    end
    
    def create
    @booking=Booking.new(booking_params)
        if @booking.save
            redirect_to booking_path(id: @booking.id)
        else
            render '/users/show'
        end
    end
    
    
    def booking_params
        params.require(:booking).permit(:product_id, :date, :adult_number, :child_number, :infant_number)
    end
    
end

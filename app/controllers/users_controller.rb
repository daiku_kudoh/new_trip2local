class UsersController < ApplicationController


  def index
    @product = Product.all
  end

  def list
    @product = Product.all
  end

  def show
    @product=Product.includes(:stocks).find(params[:id])
    gon.mindate=@product.book_start
    gon.maxdate=@product.book_finish
    @holiday=@product.stocks.where(stock:0)
    gon.holidays=@holiday.map{|holiday|holiday.date.strftime('%Y-%m-%d')}
    
    @datedetil=params[:date]
    @booking=Booking.new
  end

  def category
      @product = Product.all
      # respond_to do |format|
      #   format.js
      # end
  end

  def payment
  end
end

class ReserversController < ApplicationController
  def index
  end

  def show
    @reserver=Reserver.includes(:passengers, booking: :product).find(params[:id])
    @total=@reserver.total_price
  end

  def create
    @reserver = Reserver.new(reserver_params)
    @bookings=params[:booking_id]
    if @reserver.save
      redirect_to reserver_path(id:@reserver.id)
    else 
      redirect_to "/bookings/#{@reserver.booking_id}", flash:{error: @reserver.errors.full_messages}
end
  end
  
  private
  def reserver_params
    params.require(:reserver).permit(:first_name,
                                     :last_name,
                                     :gender,
                                     :birth,
                                     :nationality,
                                     :phone,
                                     :email,
                                     :booking_id,
                                     :passport,
                                     
    passengers_attributes:[:first_name,
                           :last_name,
                           :gender,
                           :birth,
                           :passport
  
    ])
  end
  
end

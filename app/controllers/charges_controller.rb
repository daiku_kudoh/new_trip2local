class ChargesController < ApplicationController
    
def new
end

def create
    
  # Amount in cents
  @amount = params[:price]
  @product_name  = params[:product_name]
  @reserver_first_name  = params[:reserver_first_name]
  @reserver_last_name  = params[:reserver_last_name]
  @nationality  = params[:nationality]
  @phone  = params[:phone]
  @email  = params[:email]
  @adult_number  = params[:adult_number]
  @child_number  = params[:child_number]
  @infant_number  = params[:infant_number]
  @id=params[:id]
  
  @reserver=Reserver.includes(:passengers, booking: :product).find(@id)


  customer = Stripe::Customer.create({
    email: params[:stripeEmail],
    source: params[:stripeToken],
  })

  charge = Stripe::Charge.create({
    customer: customer.id,
    amount: @amount,
    description: @product_name,
    currency: 'jpy',
    metadata:{
      product_name:@product_name ,
      reserver_first_name:@reserver_first_name ,
      reserver_last_name:@reserver_last_name ,
      nationality:@nationality ,
      phone:@phone ,
      email:@email ,
      adult_number:@adult_number ,
      child_number:@child_number ,
      infant_number:@infant_number
    }
  })

rescue Stripe::CardError => e
  flash[:error] = e.message
  redirect_to new_charge_path
end
    
    
    
    
end

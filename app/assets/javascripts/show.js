$(function () {
  // モバイル
  var mediaQuery = matchMedia('(max-width: 480px)');
  handle(mediaQuery);
  mediaQuery.addListener(handle);

  function handle(mq) {
    if (mq.matches) {
      $(function () {
        $('.mobile_menu').click(function () {
          if ($('.mobile_header_menu ul').hasClass("close")) {
            $('.mobile_header_menu ul').slideDown(300);
            $('.mobile_header_menu ul').removeClass("close");
          } else {
            $('.mobile_header_menu ul').addClass("close");
            $('.mobile_header_menu ul').slideUp(300);
          }
        })
      });
    };
    if (mq.matches) {
      $(function () {
        $('#datepicker').datepicker({
          autoclose: true,
          defaultViewDate: Date(),
          dateFormat: 'yy-mm-dd',
          minDate: gon.mindate, //ここにproductのbook start入れる
          maxDate: gon.maxdate, //ここにproductのbook end入れる
          numberOfMonths: 1, // 3ヶ月分表示
          showCurrentAtPos: 0, // 表示位置は左から2番目 (真ん中)
          stepMonths: 3, // 月の移動を3ヶ月単位とする
          showButtonPanel: true, // ボタン領域を表示
        });
      });
    } else {
      $('#datepicker').datepicker({
        autoclose: true,
        defaultViewDate: Date(),
        dateFormat: 'yy-mm-dd',
        minDate: gon.mindate, //ここにproductのbook start入れる
        maxDate: gon.maxdate, //ここにproductのbook end入れる
        numberOfMonths: 3, // 3ヶ月分表示
        showCurrentAtPos: 0, // 表示位置は左から2番目 (真ん中)
        stepMonths: 3, // 月の移動を3ヶ月単位とする
        showButtonPanel: true, // ボタン領域を表示
      });
    }
  }
  // カレンダー日付取得    
  $('#datepicker').change(function () {
    $('#datesubmit').trigger('click');
    var date = $('.date_input').val();
    $('#date').val(date);
  })
  // select the date の移動
  var position = $(".calender").offset().top;
  $(".date").click(function () {
    $('html,body').animate({
      scrollTop: position
    }, 'slow');
    return false;
  })
  // 選択ボックス
  $(".date_input").change(function () {
    $(".numbers").removeClass("numbers");
    $(".package_details").css("border", "2px solid rgb(33, 181, 192)")
  });
  // // カウンター
  var adultNum = 1;
  var childNum = 0;
  var infantNum = 0;
  var adultPrice = $(".adult_price").text();
  var childPrice = $(".child_price").text();
  var infantPrice = $(".infant_price").text();
  // マイナスボタン
  $(".fa-minus").click(function () {
    if ($(this).parent().hasClass("adult_number")) {
      if (adultNum < 2) {
        $(".adult_number span").text();
      } else {
        adultNum--;
        $(".adult_number span").text(adultNum);
        var adultTotal = adultPrice * adultNum;
        var childTotal = childPrice * childNum;
        var infantTotal = infantPrice * infantNum;
        $(".adult_price").text(adultTotal);
        $(".total_price").text(adultTotal + childTotal + infantTotal);
        $(".booking_details h1").text(adultTotal + childTotal + infantTotal);
        $("#adult_number").val(adultNum);
      }
    } else if ($(this).parent().hasClass("child_number")) {
      if (childNum < 1) {
        $(".child_number span").text();
      } else {
        childNum--;
        $(".child_number span").text(childNum);
        var adultTotal = adultPrice * adultNum;
        var childTotal = childPrice * childNum;
        var infantTotal = infantPrice * infantNum;
        $(".child_price").text(childTotal);
        $(".total_price").text(adultTotal + childTotal + infantTotal);
        $(".booking_details h1").text(adultTotal + childTotal + infantTotal);
        $("#child_number").val(childNum);
      }
    } else if ($(this).parent().hasClass("infant_number")) {
      if (infantNum < 1) {
        $(".infant_number span").text();
      } else {
        infantNum--;
        $(".infant_number span").text(infantNum);
        var adultTotal = adultPrice * adultNum;
        var childTotal = childPrice * childNum;
        var infantTotal = infantPrice * infantNum;
        $(".infant_price").text(infantTotal);
        $(".total_price").text(adultTotal + childTotal + infantTotal);
        $(".booking_details h1").text(adultTotal + childTotal + infantTotal);
        $("#infant_number").val(infantNum);
      }
    }
  });
  // プラスボタン
  $(".fa-plus").click(function () {
    if ($(this).parent().hasClass("adult_number")) {
      adultNum = adultNum + 1;
      $(".adult_number span").text(adultNum);
      var adultTotal = adultPrice * adultNum;
      var childTotal = childPrice * childNum;
      var infantTotal = infantPrice * infantNum;
      $(".adult_price").text(adultTotal);
      $(".total_price").text(adultTotal + childTotal + infantTotal);
      $(".booking_details h1").text(adultTotal + childTotal + infantTotal);
      $("#adult_number").val(adultNum);
    } else if ($(this).parent().hasClass("child_number")) {
      childNum++;
      $(".child_number span").text(childNum);
      var adultTotal = adultPrice * adultNum;
      var childTotal = childPrice * childNum;
      var infantTotal = infantPrice * infantNum;
      $(".child_price").text(childTotal);
      $(".total_price").text(adultTotal + childTotal + infantTotal);
      $(".booking_details h1").text(adultTotal + childTotal + infantTotal);
      $("#child_number").val(childNum);
    } else if ($(this).parent().hasClass("infant_number")) {
      infantNum++;
      $(".infant_number span").text(infantNum);
      var adultTotal = adultPrice * adultNum;
      var childTotal = childPrice * childNum;
      var infantTotal = infantPrice * infantNum;
      $(".infant_price").text(infantTotal);
      $(".total_price").text(adultTotal + childTotal + infantTotal);
      $(".booking_details h1").text(adultTotal + childTotal + infantTotal);
      $("#infant_number").val(infantNum);
    }
  });
  // カウンタ終わり
  // Bookボタン無効
  //始めにjQueryで送信ボタンを無効化する
  $('.input_btn').prop("disabled", true);
  //入力欄の操作時
  $('#datepicker').change(function () {
    //必須項目が空かどうかフラグ
    let flag = true;
    //必須項目をひとつずつチェック
    $('form input:required').each(function (e) {
      //もし必須項目が空なら
      if ($('form input:required').eq(e).val() === "") {
        flag = false;
      }
    });
    //全て埋まっていたら
    if (flag) {
      //送信ボタンを復活
      $('.input_btn').prop("disabled", false);
    } else {
      //送信ボタンを閉じる
      $('.input_btn').prop("disabled", true);
    }
  });
})
class Reserver < ApplicationRecord
    belongs_to :booking
    has_many :passengers, dependent: :destroy
    accepts_nested_attributes_for :passengers

  before_save { self.email = email.downcase }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: {with: VALID_EMAIL_REGEX}
  validates :first_name, :last_name, :phone, presence: true
#   validates :passport, length: { minimum: 3 }
    
        
    def total_price
        self.booking.product.adult_price * self.booking.adult_number +
        self.booking.product.child_price * self.booking.child_number  +
        self.booking.product.infant_price  * self.booking.infant_number 
    end
    
end

class Product < ApplicationRecord
    
      mount_uploader :image1, ImageUploader 
      mount_uploader :image2, ImageUploader 
      mount_uploader :image3, ImageUploader 
      mount_uploader :image4, ImageUploader 
      mount_uploader :image5, ImageUploader 
      #記述を追加
      
      has_many :stocks
      
      has_many :bookings
      has_many :reservers, through: :bookings
    
end

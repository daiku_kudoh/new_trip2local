class Booking < ApplicationRecord
    belongs_to :product
    has_many :reservers
    validates :adult_number, numericality:{greater_than_or_equal_to: 1},
              presence:true
    validates :date, presence:true

    
    
    
    # 自作メソッド
    def number
        self.adult_number+self.child_number+self.infant_number-1
    end
    


end

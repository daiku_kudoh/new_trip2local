ActiveAdmin.register Product do


  permit_params :name, :max_stock, :duration, 
                :min_cancel, :simple_explanation,:highlight, 
                :course, :important, :about_tour,:additional_Information, 
                :itinerary, :include_price,
                :cancellation_policy, :pickup, :dropoff, 
                :book_start,
                :book_finish, :prefecture, 
                :image1, :image2, :image3, :image4, :image5, :adult_price, :child_price, :infant_price
                
    form do |f|
      f.inputs "Products" do
        f.input :name
        f.input :adult_price
        f.input :child_price
        f.input :infant_price
        f.input :max_stock
        f.input :duration
        f.input :min_cancel
        f.input :simple_explanation
        f.input :highlight
        f.input :course
        f.input :important
        f.input :about_tour
        f.input :additional_Information
        f.input :itinerary
        f.input :include_price
        f.input :cancellation_policy
        f.input :pickup
        f.input :dropoff
        f.input :book_start
        f.input :book_finish
        f.input :prefecture
        f.input :image1, :as => :file
        f.input :image2, :as => :file
        f.input :image3, :as => :file
        f.input :image4, :as => :file
        f.input :image5, :as => :file
      end
      f.actions
    end
    
    show do |item_image|
      attributes_table do
        row :name
        row :adult_price
        row :child_price
        row :infant_price
        row :max_stock
        row :duration
        row :min_cancel
        row :simple_explanation
        row :highlight
        row :course
        row :important
        row :about_tour
        row :additional_Information
        row :itinerary
        row :include_price
        row :cancellation_policy
        row :pickup
        row :dropoff
        row :book_start
        row :book_finish
        row :prefecture
        
        # show画面で画像を表示するためのタグを追加
        row :image do
          image_tag(product.image1.url)
        end
        row :image do
          image_tag(product.image2.url)
        end
        row :image do
          image_tag(product.image3.url)
        end
        row :image do
          image_tag(product.image4.url)
        end
        row :image do
          image_tag(product.image5.url)
        end
      end
    end    


end

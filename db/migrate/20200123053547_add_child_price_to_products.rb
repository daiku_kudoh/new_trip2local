class AddChildPriceToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :child_price, :integer
    add_column :products, :infant_price, :integer
  end
end

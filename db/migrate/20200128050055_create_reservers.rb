class CreateReservers < ActiveRecord::Migration[5.1]
  def change
    create_table :reservers do |t|
      t.integer :booking_id
      t.string :first_name
      t.string :last_name
      t.integer :age

      t.timestamps
    end
  end
end

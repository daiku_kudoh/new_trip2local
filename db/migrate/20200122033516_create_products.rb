class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.integer :max_stock
      t.time :duration
      t.string :min_cancel
      t.text :simple_explanation
      t.text :highlight
      t.text :course
      t.text :important
      t.text :about_tour
      t.text :additional_Information
      t.text :itinerary
      t.text :include_price
      t.text :cancellation_policy
      t.string :pickup
      t.string :dropoff
      t.date :book_start
      t.date :book_finish
      t.string :prefecture

      t.timestamps
    end
  end
end

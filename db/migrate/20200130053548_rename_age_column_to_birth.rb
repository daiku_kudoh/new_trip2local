class RenameAgeColumnToBirth < ActiveRecord::Migration[5.1]
  def change
        rename_column :reservers, :age, :birth
  end
end

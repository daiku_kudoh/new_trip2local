class RemoveReserverIdFromBookings < ActiveRecord::Migration[5.1]
  def change
    remove_column :bookings, :reserver_id, :integer
  end
end

class RenamePriceColumnToAdultprice < ActiveRecord::Migration[5.1]
  def change
    rename_column :products, :price, :adult_price
  end
end

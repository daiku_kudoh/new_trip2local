class AddPassportToReserver < ActiveRecord::Migration[5.1]
  def change
    add_column :reservers, :passport, :string
  end
end

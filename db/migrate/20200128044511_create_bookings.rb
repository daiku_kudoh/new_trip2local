class CreateBookings < ActiveRecord::Migration[5.1]
  def change
    create_table :bookings do |t|
      t.integer :product_id
      t.date :date
      t.integer :adult_number
      t.integer :child_number
      t.integer :infant_number

      t.timestamps
    end
  end
end

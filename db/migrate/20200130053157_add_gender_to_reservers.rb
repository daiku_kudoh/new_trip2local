class AddGenderToReservers < ActiveRecord::Migration[5.1]
  def change
    add_column :reservers, :gender, :integer
    add_column :reservers, :nationality, :string
    add_column :reservers, :phone, :string
    add_column :reservers, :email, :string
  end
end

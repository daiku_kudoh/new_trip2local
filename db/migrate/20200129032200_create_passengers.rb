class CreatePassengers < ActiveRecord::Migration[5.1]
  def change
    create_table :passengers do |t|
      t.string :first_name
      t.string :last_name
      t.integer :gender
      t.date :birth
      t.string :passport

      t.timestamps
    end
  end
end

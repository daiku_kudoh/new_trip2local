# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200130054243) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.integer "resource_id"
    t.string "author_type"
    t.integer "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "bookings", force: :cascade do |t|
    t.integer "product_id"
    t.date "date"
    t.integer "adult_number"
    t.integer "child_number"
    t.integer "infant_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "passengers", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.integer "gender"
    t.date "birth"
    t.string "passport"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "reserver_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.integer "max_stock"
    t.time "duration"
    t.string "min_cancel"
    t.text "simple_explanation"
    t.text "highlight"
    t.text "course"
    t.text "important"
    t.text "about_tour"
    t.text "additional_Information"
    t.text "itinerary"
    t.text "include_price"
    t.text "cancellation_policy"
    t.string "pickup"
    t.string "dropoff"
    t.date "book_start"
    t.date "book_finish"
    t.string "prefecture"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image1"
    t.string "image2"
    t.string "image3"
    t.string "image4"
    t.string "image5"
    t.integer "adult_price"
    t.integer "child_price"
    t.integer "infant_price"
  end

  create_table "reservers", force: :cascade do |t|
    t.integer "booking_id"
    t.string "first_name"
    t.string "last_name"
    t.integer "birth"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "gender"
    t.string "nationality"
    t.string "phone"
    t.string "email"
    t.string "passport"
  end

  create_table "stocks", force: :cascade do |t|
    t.integer "product_id"
    t.date "date"
    t.integer "stock"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end

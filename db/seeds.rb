require "csv"

CSV.foreach('db/seeds/product_seed.csv', headers: true) do |row|
  Product.create(
    name: row['name'],
    max_stock: row['max_stock'],
    duration: row['duration'],
    min_cancel: row['min_cancel'],
    simple_explanation: row['simple_explanation'],
    highlight: row['highlight'],
    course: row['course'],
    important: row['important'],
    about_tour: row['about_tour'],
    additional_Information: row['additional_Information'],
    itinerary: row['itinerary'],
    include_price: row['include_price'],
    cancellation_policy: row['cancellation_policy'],
    pickup: row['pickup'],
    dropoff: row['dropoff'],
    book_start: row['book_start'],
    book_finish: row['book_finish'],
    prefecture: row['prefecture'],
    image1: row['image1'],
    image2: row['image2'],
    image3: row['image3'],
    image4: row['image4'],
    image5: row['image5'],
    adult_price: row['adult_price'],
    child_price: row['child_price'],
    infant_price: row['infant_price']
  )
end

AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') 

CSV.foreach('db/seeds/stock_seed.csv', headers: true) do |row|
  Stock.create(
    product_id: row['product_id'],
    date: row['date'],
    stock: row['stock']
  )
end
